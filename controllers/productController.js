const Product = require("../models/Product");


//add a product
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price		
	})

	return newProduct.save().then(result => true)
		.catch(err => {
			console.log(err);
			return false;
		})
};


//retrieve all active products
module.exports.getActiveProducts = () => {

	return Product.find({isActive : true}).sort({createdOn: 'descending'}).then(result => result)
		.catch(err => {
			console.log(err);
			return false;
		})
};

//retrieve all products active or not
module.exports.getAllProducts = () => {
	return Product.find({}).sort({createdOn: 'descending'}).then(result => result)
		.catch(err => {
			console.log(err);
			return false;
		})
};

//retrieve a specific product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => result)
		.catch(err => {
			console.log(err);
			return false;
		})
};


//update the product info
module.exports.updateProduct = (productId,reqBody) => {

	return Product.findById(productId).then(result => {
		result.name = reqBody.name;
		result.description = reqBody.description;
		result.price = reqBody.price;

		//return re

		return result.save().then(result => true)
			.catch(err => {
				console.log(err);
				return false;
			})

	}).catch(err => {
		console.log(err);
		return false;
	})
};

//archive product
module.exports.archiveProduct = (productId) => {

	return Product.findByIdAndUpdate(productId,{isActive: false}).then(result=> true)
		.catch(err => {
			console.log(err);
			return false;
		})
};

//activate product
module.exports.activateProduct = (productId) => {
	return Product.findByIdAndUpdate(productId,{isActive: true}).then(result=> true)
		.catch(err => {
			console.log(err);
			return false;
		})
}

module.exports.getMultipleProducts = (data) => {
	let productIdArray = data.map(x => x.productId);

	return Product.find({'_id': { $in: productIdArray}}).then(result => result)
		.catch(err => {
			console.log(err);
			return false
		})
}