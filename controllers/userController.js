const bcrypt = require("bcrypt");
const mongoose = require("mongoose");

const auth = require("../auth");
const User = require("../models/User.js");
const Product = require("../models/Product.js");


//Check Email 
//edited to be used as middleware in the register route
module.exports.checkEmailExists = (reqbody) => {
	

	//console.log(req.email);
	return User.find({email : reqbody.email})
		.then(result => {
			if(result.length > 0){

				//return true;
				console.log("email already exists")
				//return res.send(false);
				return true;

			} else {

				//return false;
				console.log("email does not exist")
				//next();
				return false;
			}
		})
		.catch(err => {
			console.log(err);
			//return res.send(false);
			return false;
		})
};



//Register a User
module.exports.registerUser = (reqBody) => {

	
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		//mobileNo : reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save()
		.then(user => true)
		.catch(err => {
			console.log(err);
			return false;
		});
};



//User login
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email})
	.then(result => {

		if(result == null){

			console.log("email not found");
			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			//if passwords match
			if(isPasswordCorrect){

				return { access : auth.createAccessToken(result)}
			} else {

				console.log("incorrect password");
				return false; //password did not match
			}
		}
	})
	.catch(err => {
		console.log(err);
		return false;
	})
};


//retrieve user detail
module.exports.getUserDetail = (userId) => {

	return User.findById(userId)
		.then(result => {
			result.password = "";
			return result;
		})
		.catch(err => {
			console.log(err);
			return false;
		})
};


//order
// edited for multiple product checkout
module.exports.order =  async (userId,data) => {

	//
	let productDetails;
	productIdArray = data.map(x => x.productId);
	
	let isProductDetailRetrieved = await Product.find({'_id': { $in: productIdArray}})
		.then(result => {
			productDetails = result
			console.log(result);
			return true
		}).catch(err => {
			console.log(err);
			return false;
		});
	
	let orderUpdates = [];
	let userProductArr = [];
	let compAmount = 0

	for (let i = 0 ; i < data.length ; i++) {
		orderUpdates.push({
			updateOne: {
				filter: { _id: data[i].productId },
				update:{
					$push: { userOrders:{userId:userId}}
				}
			}
		})

		//console.log(i);
		userProductArr.push({
			productId: data[i].productId,
			productName: productDetails[i].name,
			quantity: data[i].quantity
		})

		compAmount += (data[i].quantity * productDetails[i].price);

	}


	console.log(compAmount);
	console.log(userProductArr);
	console.log(orderUpdates[0].updateOne.filter);

	let isProductUpdated = await Product.bulkWrite(orderUpdates).then(result => {
		console.log(result.modified);
		return true
	})
	
		.catch(err => {
			console.log(err);
			return false;
		})


	let isUserUpdated = await User.findById(userId)
		.then(user => {
			user.orderedProduct.push(
				{
					products : userProductArr,

					totalAmount : compAmount


				}
			);

			return user.save().then(result => true)
				.catch(err => {
					console.log(err);
					return false;
				})


		}).catch(err => {
			console.log(err);
			return false;
		})

	/*let productDetail;
	


	



	let isUserUpdated = await User.findById(data.userId)
		.then(user => {
			user.orderedProduct.push(
				{
					products : [

							{
								productId : data.productId,
								productName: productDetail.name,
								quantity: data.quantity
							}

						],

					totalAmount : productDetail.price * data.quantity


				}
			);

			return user.save().then(result => true)
				.catch(err => {
					console.log(err);
					return false;
				})


		}).catch(err => {
			console.log(err);
			return false;
		})


	if(isUserUpdated && isProductUpdated){
		console.log("successful");
		return true;
	} else {
		console.log("unsuccessful");
		return false;
	}*/


	//return isProductDetailRetrieved;
	if(isUserUpdated && isProductUpdated && isProductDetailRetrieved){
		console.log("successful");
		return true;
	} else {
		console.log("unsuccessful");
		return false;
	}

};


module.exports.getAllOrders =()=> {

	return User.find({},"orderedProduct").then(result => result)
		.catch(err => {
			console.log(err);
			return false;
		})
};

module.exports.getUserOrders =(userId)=>{
	return User.findById(userId,"orderedProduct").then(result => result)
		.catch(err => {
			console.log(err);
			return false;
		})
};

module.exports.setAsAdmin =(reqBody)=>{
	return User.findOneAndUpdate(reqBody,{isAdmin: true}).then(result=> true)
		.catch(err => {
			console.log(err);
			false;
		})
};