const jwt = require("jsonwebtoken");
const secret = "E-CommerceAPI";


//access token
module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}

	return jwt.sign(data, secret, {});
};


//Verifying Toeken
module.exports.verify = (req,res,next) => {

	
	let token = req.headers.authorization

	// Token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		
		token = token.slice(7, token.length);

		
		return jwt.verify(token, secret,(err,data) => {

			console.log(data);

			
			if(err){
				return res.send({ auth: "failed"});

			
			} else {

				
				next();
			}
		})

	} else {

		return res.send({auth:"failed"});
	}
};


//decoding info from toeken
module.exports.decode = (token) => {

	//token received 
	if(typeof token !== "undefined"){

		token = token.slice(7,token.length);

		return jwt.verify(token,secret,(err,data)=> {
			

			if(err){
				return null
			} else {

				
				return jwt.decode(token,{complete:true}).payload
			}
		})

	//token does not exist
	} else {
		return null
	}
};