const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,		
		required : [true, "Course is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true, 
	},
	createdOn : {
		type : Date,
		// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type : mongoose.Schema.Types.ObjectId,
				required : [true,"User Id is required"]
			},

			//optional??
			orderId : {
				type : String

			}
		}

	]
})



module.exports = mongoose.model("Product", productSchema);