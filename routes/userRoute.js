const express = require("express");
const router = express.Router();

const auth = require("../auth.js")

const userController = require("../controllers/userController.js");



router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body)
		.then(resultFromController => res.send(resultFromController))
});


//router for user registration
router.post("/register", (req,res) => {

	userController.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController))
});

//login
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//router for retrieving user detail
router.post("/details", auth.verify,(req,res) => {

	const userData = auth.decode(req.headers.authorization);
	//{userId: userData.id}
	userController.getUserDetail(userData.id).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout",auth.verify,(req,res)=> {
	const userData = auth.decode(req.headers.authorization); 


	if(userData.isAdmin == true ){
		console.log("user is admin");
		return  res.send(false);
	} else {

		userController.order(userData.id,req.body).then(resultFromController => res.send(resultFromController));
	}

	
});

router.get("/orders",auth.verify,(req,res)=> {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin");
		return  res.send(false);
	} else {

		userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	}
})

router.get("/myOrders",auth.verify,(req,res)=> {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true ){
		console.log("user is admin");
		return  res.send(false);
	} else {

		userController.getUserOrders(userData.id).then(resultFromController => res.send(resultFromController));
	}
});

router.put("/:userId/setAsAdmin",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin");
		return  res.send(false);
	} else {

		userController.setAsAdmin(req.body).then(resultFromController => res.send(resultFromController));
	}
});

module.exports = router;