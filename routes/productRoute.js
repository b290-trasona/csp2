const express = require("express");
const router = express.Router();

const auth = require("../auth.js");

const productController = require("../controllers/productController");


//router for adding product (for admin only) 
router.post("/", auth.verify, (req,res) =>{

	let userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin")
		return  res.send(false);
	} else {

		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}

	
});


//router for retrieving all active products
router.get("/", (req,res) => {

	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
});

//router for getting all products(active or not) for admin only
router.get("/all",auth.verify,(req,res) => {

	let userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin")
		return  res.send(false);
	} else {

		productController.getAllProducts().then(resultFromController => res.send(resultFromController))
	}

});


//retrieve specific product
router.get("/:productId", (req,res) => {

	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
});

//update product info
router.put("/:productId", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin");
		return  res.send(false);
	} else {

		productController.updateProduct(req.params.productId,req.body).then(resultFromController => res.send(resultFromController));
	}
});


//archives the product
router.put("/:productId/archive", auth.verify,(req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin");
		return  res.send(false);
	} else {

		productController.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
	}
});

//activate the product
router.put("/:productId/activate", auth.verify, (req,res)=> {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin !== true ){
		console.log("user is not admin");
		return  res.send(false);
	} else {

		productController.activateProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
	}
})

router.post("/multiple",auth.verify, (req,res)=> {

	productController.getMultipleProducts(req.body).then(resultFromController => res.send(resultFromController));

})


module.exports = router;